import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({label, onPress}) => {
    const { buttonStyle, textStyle } = styles;
    return (
        <TouchableOpacity onPress= {onPress} style= {buttonStyle}>
            <Text style= {textStyle}> {label} </Text>
        </TouchableOpacity>
    );
};

const styles = {
    textStyle: {
        alignSelf: 'center',
        color: 'azure',
        fontSize: 20,
        paddingTop: 10,
        paddingBottom: 10
    },
    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: 'black',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5
    }
};

export { Button };
