import firebase from '@firebase/app';
import '@firebase/auth';
import React, {Component} from 'react';
import { Text } from 'react-native';
import { Button, Card, CardSection, Input, Spinner } from './common';

class LoginForm extends Component{
    state = { email: '', pass: '', error: '', loading: false }

    onButtonPress() {
        const { email, pass } = this.state;
        this.setState({ error: '', loading: true });

        firebase.auth().signInWithEmailAndPassword(email, pass)
            .then(this.onLoginSuccess.bind(this))
            .catch(()=> {
                firebase.auth().createUserWithEmailAndPassword(email, pass)
                    .then(this.onLoginSuccess.bind(this))
                    .catch(this.onLoginFail.bind(this));
            });
    }

    onLoginFail() {
        this.setState({
            error: 'Authentication Failed.',
            loading: false
        });
    }

    onLoginSuccess() {
        this.setState({
            email: '',
            pass: '',
            error: '',
            loading: false
        });
    }

    renderButton() {
        if(this.state.loading){
            return <Spinner size= {'small'} />
        }

        return (
            <Button 
                onPress= { this.onButtonPress.bind(this) }
                label= {'Login'}>
            </Button>
        );
    }

    render(){
        return(
            <Card>

            <CardSection>
                <Input 
                    label={ 'Email' }
                    value={this.state.email}
                    onChangeText={email => this.setState({ email }) }
                    placeholder= { 'example@domain.com'}
                />
            </CardSection>
            <CardSection>
                <Input 
                    secureTextEntry
                    label={ 'Password' }
                    value={this.state.pass}
                    onChangeText={pass => this.setState({ pass }) }
                    placeholder= { 'Enter password'}
                />
            </CardSection>

            <Text style= { styles.errorTextStyle }>
                { this.state.error }
            </Text>

            <CardSection>
                {this.renderButton()}
            </CardSection>

            </Card>
        );
    }
}

const styles = {
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    }
};

export default LoginForm;
