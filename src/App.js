import firebase from '@firebase/app';
import '@firebase/auth';
import React, {Component} from 'react';
import { View } from 'react-native';
import { Header, Button, Spinner, CardSection } from './components/common';
import LoginForm from './components/LoginForm'

export default class App extends Component {
  state = { loggedIn: null };

  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDg6ql8-LrJ8_-crbNz0ostIQP8z2MIs8I',
      authDomain: 'logi-6d546.firebaseapp.com',
      databaseURL: 'https://logi-6d546.firebaseio.com',
      projectId: 'logi-6d546',
      storageBucket: 'logi-6d546.appspot.com',
      messagingSenderId: '646588312104'
    });

    firebase.auth().onAuthStateChanged((user)=> { 
      if (user) {
        this.setState({ loggedIn: true });
      } else {
        this.setState({ loggedIn: false });
      }
    });
  }

  renderContent(){
    switch (this.state.loggedIn) {
      case true: 
        return (
          <CardSection>
            <Button 
              label= {'Log Out'} 
              onPress= {()=> firebase.auth().signOut()} />
          </CardSection>
        );
      case false: 
        return <LoginForm />;
      default: 
        return (
          <View style= {{flex: 1, justifyContent: 'center'}}>
            <Spinner size= {'large'} />
          </View>
        );
    }    
  }

  render() {
    return (
      <View style= {{flex: 1, backgroundColor: 'azure'}}>
        <Header headerText={ 'Logi' } />
        {this.renderContent()}
      </View>
    );
  }
}
