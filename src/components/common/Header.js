import React from 'react';
import { Text, View } from 'react-native';

const Header = ({ headerText }) =>{
    const { textStyle, viewStyle } = styles;

    return (
        <View style={viewStyle}>
            <Text style={textStyle}> { headerText } </Text>
        </View>
    );
};

const styles = {
    viewStyle:{
        flexDirection: 'row',
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItem: 'center',
        height: 70,
        paddingTop: 15,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        position: 'relative'
    },
    textStyle:{
        fontSize: 35,
        color: 'floralwhite',
        textAlign:'center'
    }
};

export { Header };